﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;

namespace SkyInTest.Models
{
    public class User
    {
        public User()
        {
            Telefones = new List<Telefone>();
        }
        public Guid Id { get; internal set; }
        public DateTime UltimoLogin { get; internal set; }
        public JwtSecurityToken Token { get; internal set; }
        public DateTime DataCriacao { get; internal set; }

        public List<Telefone> Telefones { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
