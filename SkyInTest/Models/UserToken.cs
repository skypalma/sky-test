﻿using System;


namespace SkyInTest.Models
{
    public class UserToken
    {
        public string Token{ get; set; }
        public  DateTime Expiration { get; set; }
    }
}
