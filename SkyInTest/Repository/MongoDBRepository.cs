﻿using MongoDB.Driver;
using SkyInTest.Contracts;
using System;


namespace SkyInTest.Repository
{
    public class MongoDbRepository : IDisposable, IMongoDbRepository
    {
        public MongoDbRepository()
            : this("localhost")
        {

        }

        public MongoDbRepository(string databaseName)
        {
            DatabaseName = databaseName;
            _client = new MongoClient();
            _database = _client.GetDatabase(DatabaseName);
        }

        public void Dispose()
        {
            _client = null;
            _database = null;
        }

        public string DatabaseName { get; set; }
        private IMongoClient _client;
        private IMongoDatabase _database;

        public IMongoDatabase Database
        {
            get
            {
                return _database;
            }
        }
    }
}
