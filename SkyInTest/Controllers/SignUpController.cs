﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SkyInTest.Contracts;
using SkyInTest.Models;
using System.Threading.Tasks;

namespace SkyInTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SignUpController : ControllerBase
    {

        private readonly IUserService _userService;
        public SignUpController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("Create")]
        [AllowAnonymous]
        public async Task<ActionResult<User>> Create([FromBody] User model)
        {
            try
            {
                var result = await _userService.InsertUser(model);

                return result;
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
