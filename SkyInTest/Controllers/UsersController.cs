﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using SkyInTest.Context;
using SkyInTest.Contracts;
using SkyInTest.Models;
using SkyInTest.Services;

namespace SkyInTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService { get; set; }
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        [HttpGet]
        public ActionResult<User> Get(Guid idUsuario)
        {
            return _userService.GetUser(idUsuario);
        }
    }
}
