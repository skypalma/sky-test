﻿using MongoDB.Driver;
using SkyInTest.Context;
using SkyInTest.Contracts;
using SkyInTest.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SkyInTest.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;
        public UserService(ApplicationDbContext context)
        {
            _context = context;

        }

        public User AddUser(User user)
        {
            MongoDbContext dbContext = new MongoDbContext();
            user.Id = Guid.NewGuid();
            user.DataCriacao = DateTime.Now;
            user.UltimoLogin = DateTime.Now;
            dbContext.Users.InsertOne(user);
            return user;
        }


        public bool UpdateUser(User user)
        {
            MongoDbContext dbContext = new MongoDbContext();

            dbContext.Users.ReplaceOne(m => m.Email == user.Email, user);

            return true;
        }

        public User GetUser(Guid userId)
        {
            MongoDbContext dbContext = new MongoDbContext();

            var model = dbContext.Users.Find(m => m.Id == userId);
            return model.FirstOrDefault();

        }

        private bool UserExists(User user)
        {
            return _context.Users.Any(m => m.Email == user.Email && m.PasswordHash == MD5Hash.Hash.Content(user.Password));
        }

        private bool EmailExists(string email)
        {
            return _context.Users.Any(m => m.Email == email);
        }

        public bool Validate(string email, string password)
        {
            var user = new User();
            user.Email = email;
            user.Password = password;

            return UserExists(user);
        }

        public async Task<User> InsertUser(User model)
        {
            if (EmailExists(model.Email))
            {
                throw new ArgumentException(
           $"E-mail já existente");
            }

            return AddUser(model);
        }
        
    }
}
