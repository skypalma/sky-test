﻿using SkyInTest.Models;
using System;
using System.Threading.Tasks;

namespace SkyInTest.Contracts
{
    public interface IUserService
    {
        public User AddUser(User user);

        public bool UpdateUser(User user);

        public User GetUser(Guid userId);

        bool Validate(string email, string password);        
        Task<User> InsertUser(User model);
    }
}
