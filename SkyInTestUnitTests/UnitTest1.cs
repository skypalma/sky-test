using Microsoft.AspNetCore.Mvc;
using Moq;
using SkyInTest.Contracts;
using SkyInTest.Controllers;
using SkyInTest.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace SkyInTestUnitTests
{
    public class UnitTest1
    {

        public UnitTest1()
        {

        }

        [Fact]
        public void Test1()
        {
            var mockRepo = new Mock<IUserService>();
            var controller = new SignUpController(mockRepo.Object);
            User usuario = new User();
            usuario.Email = "felipe.bitencourtp@gmail.com";
            usuario.Password = "Sky@2020";
            var telefone = new Telefone();
            telefone.DDD = "011";
            telefone.Numero = "988844446";

            usuario.Telefones.Add(telefone);

            var result = controller.Create(usuario);

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
